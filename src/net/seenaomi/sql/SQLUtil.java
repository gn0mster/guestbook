package net.seenaomi.sql;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class SQLUtil {
	public static DataSource ds = null;
	
	public static synchronized Connection getConnection() throws SQLException, NamingException {
		if (ds == null) {
			InitialContext ctx = new InitialContext();
			ds = (DataSource)ctx.lookup("java:comp/env/jdbc/guestbook");
		}
		return ds.getConnection();
	}
}
