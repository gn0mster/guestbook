package net.seenaomi.guestbook.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import net.seenaomi.guestbook.beans.Guestbook;

/**
 * Servlet implementation class Guestbook
 */

public class GuestbookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public GuestbookServlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response); // always make your doGet call doPost

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String v = request.getParameter("start");
		try {
			if (v != null) {
				getGuestbookEntries(request, response);
			}
			else {
				v = request.getParameter("author");
				if (v != null) {
					signGuestbook(request, response);
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
			JSONObject json = new JSONObject();
			json.put("error", "Something went wrong! Try again later!");
			PrintWriter writer = response.getWriter();
			writer.print(json.toString());
		}
	}
	
	private void signGuestbook(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Guestbook entry = new Guestbook();
		String v = request.getParameter("author");
		entry.setAuthor(v);
		v = request.getParameter("location");
		entry.setLocation(v);
		v = request.getParameter("email");
		entry.setEmail(v);
		v = request.getParameter("comment");
		entry.setComment(v);
		v = request.getParameter("showEmail");
		if (v != null) {
			entry.setShowEmail(v.equals("Y"));
		}
//		entry.setShowEmail("Y".equals(v));
		try {
			entry.save();
			JSONObject json = new JSONObject();
			json.put("status", "Thanks for your comment!");
			PrintWriter writer = response.getWriter();
			writer.print(json.toString());
		}
		catch (Exception e) {
			e.printStackTrace();
			JSONObject json = new JSONObject();
			json.put("error", "Could not save Guestbook entry! Try again later!");
			PrintWriter writer = response.getWriter();
			writer.print(json.toString());
		}
	}
	
	private void getGuestbookEntries(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String v = request.getParameter("start");
			int start = Integer.parseInt(v); // this makes the string into an int
			System.out.println("Get start " +start);
			v = request.getParameter("count");
			int count = Integer.parseInt(v);
			List<Guestbook> list = Guestbook.load(start, count);
			// TODO do stuff with List like turn it into JSON
			JSONArray array = new JSONArray();
			System.out.println(array.toString());
			// []
			for (Guestbook gb : list) { // for equals a loop gb is being
										// assigned to each Guestbook post once
										// there is no more posts then it stops
				JSONObject jo = new JSONObject();
				// {}
				jo.put("author", gb.getAuthor());
				// {
				// "author": "Naomi See"
				// }
				jo.put("location", gb.getLocation());
				// {
				// "location": "Omaha, Ne"
				// }
				jo.put("comment", gb.getComment());
				// {
				// "comment": "Comment1"
				// }
				jo.put("email", gb.getEmail());
				// {
				// "email": "naomi.see@seenaomi.net"
				// }
				jo.put("showEmail", gb.isShowEmail());
				// {
				// "checkbox": "if checked then email will show"
				// }
				// TODO rest of the fields go here
				array.put(jo);
				// [
				// {
				// "author": "Naomi See"
				// "location": "Omaha, Ne"
				// "comment": "Comment1"
				// "email": "naomi.see@seenaomi.net"
				// }
				// ]
			}
			String output = array.toString();
			PrintWriter writer = response.getWriter();
			writer.print(output);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
}
