package net.seenaomi.guestbook.beans;


import java.util.List;

/**
 * This interface is just setting the rules or the pattern of usage to what the other classes have to adhere by 
 * It enforces the classes that implement them have to have the below methods included
 */
public interface Databean<T> { //repeative methods can go into interfaces as a generic T = type
	public List<T> load(int start, int count);
	public void save();

}
