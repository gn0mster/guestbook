package net.seenaomi.guestbook.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.naming.NamingException;

import net.seenaomi.sql.SQLUtil;

public class Guestbook {
	// shell class needs stuff in this case vars declared from the DB
	private int id; // data by a rule is always private
	private String author;
	private String location;
	private String comment;
	private Date created; // import java.util.Date
	private String email;
	private boolean showEmail;

	// right click go to Source > Getters and Setters > Select what you want
	// make them
	// public and choose the placeholder comments option

	public static List<Guestbook> load(int start, int count)
			throws SQLException, NamingException { // constructor method that
													// returns a list of many
													// guestbook entries Source
													// > organize imports
													// list.util this is loading
													// from the DB
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Guestbook> list = new LinkedList<Guestbook>(); // LinkedList are
															// dynamically sized
															// (array list are
															// great if you need
															// to get something
															// random or
															// specific)
															// LinkedLists are
															// linear and always
															// start from the
															// beginning
		try {
			conn = SQLUtil.getConnection();
			ps = conn
					.prepareStatement("SELECT * from Guestbook ORDER BY created desc LIMIT ? OFFSET ?");
			int n = 1;
			ps.setInt(n++, count);
			ps.setInt(n++, start);
			rs = ps.executeQuery();
			while (rs.next()) {
				Guestbook gb = new Guestbook();
				gb.setAuthor(rs.getString("author"));
				gb.setLocation(rs.getString("location"));
				gb.setComment(rs.getString("comment"));
				gb.setEmail(rs.getString("email"));
				gb.setShowEmail("Y".equals(rs.getString("showEmail")));
				list.add(gb);
			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				}
				catch (SQLException e) {					
				}
			}
			if (ps != null) {
				try {
					ps.close();
				}
				catch (Exception e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				}
				catch (Exception e) {
				}
			}
		}
		return list;
	}

	public void save() throws SQLException, NamingException { // saving this guestbook entry that's filled in with
							// data to the DB
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = SQLUtil.getConnection();
			String sql = "INSERT INTO Guestbook (author, location, email, showEmail, comment) VALUES(?,?,?,?,?)";
			System.out.println("SQL: " + sql);
			ps = conn.prepareStatement(sql/*, PreparedStatement.RETURN_GENERATED_KEYS*/);
			int n = 1;
			ps.setString(n++, author);
			ps.setString(n++, location);
			ps.setString(n++, email);
			ps.setString(n++, showEmail ? "Y" : "N");
			ps.setString(n++, comment);
			ps.execute();
//			ResultSet rs = ps.getGeneratedKeys();
//			if (rs != null && rs.next()) {
//				id = rs.getInt(1);
//			}
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
				}
			}
			if (ps != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}
	}

	/**
	 * Gets the primary key for this Guestbook
	 * 
	 * @return the id primary key
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set a parm is a local var
	 */
	public void setId(int id) {
		this.id = id; // data hiding = this.id is referring to the var id
						// declared globally not locally id = theid is dealing
						// with a local var to the method
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the showEmail
	 */
	public boolean isShowEmail() {
		return showEmail;
	}

	/**
	 * @param showEmail
	 *            the showEmail to set
	 */
	public void setShowEmail(boolean showEmail) {
		this.showEmail = showEmail;
	}

}
