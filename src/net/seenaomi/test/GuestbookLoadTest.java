package net.seenaomi.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import net.seenaomi.guestbook.beans.Guestbook;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GuestbookLoadTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Ensure that the Guestbook table has data before this is ran!
	 * @throws NamingException 
	 * @throws SQLException 
	 */
	@Test
	public void testFirstPage() throws SQLException, NamingException {
		List<Guestbook> list = Guestbook.load(0, 20);
		if (list == null || list.isEmpty()) {
			fail("List empty");
		}
	}
	
	@Test
	public void testSecondPage() throws SQLException, NamingException {
		List<Guestbook> list = Guestbook.load(20, 20);
		if (list == null || list.isEmpty()) {
			fail("List empty");
		}
	}
}
